<?php

// src/Command/CreateUserCommand.php
namespace App\Command;

use App\Entity\DatingProfile;
use App\Entity\Media;
use App\Entity\Message;
use Faker;
use App\Entity\User;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PopulateCommand extends Command
{
    protected $output;
    protected $container;
    protected $em;

    protected $usersId = [];


    public function __construct($name = null, ContainerInterface $container) {
        parent::__construct($name);
        $this->container = $container;
    }

    protected function configure()
    {
        $this->setName('taotesting:populate')
            ->setDescription('Populate database');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->output = $output;
        $this->em = $this->container->get('doctrine')->getEntityManager();

        $this->output->writeln('##### Populate TaoTesting database with fake data #####');


        if (($handle = fopen("users.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $this->createUser($data);
            }
            $this->em->flush();
            fclose($handle);
        }

    }

    public function createUser($data) {

        $user = new User();


        $this->output->writeln('##### creating user');

        $user->setLogin($data[0]);
        $user->setPassword($data[1]);
        $user->setTitle($data[2]);
        $user->setFirstname($data[3]);
        $user->setLastname($data[3]);
        $user->setGender($data[4]);
        $user->setEmail($data[5]);
        $user->setPicture($data[6]);
        $user->setAddress($data[7]);
        $this->em->persist($user);

    }



}