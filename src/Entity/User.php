<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * User
 * @ORM\Entity
 * @ORM\Table(name="user", uniqueConstraints={
 *  @ORM\UniqueConstraint(name="UNIQ_8D93D64992FC23A8", columns={"login"}),
 * })

 * @UniqueEntity("username",message="form.signup.error.username.alreadyInUse")
 * @ApiResource(attributes={})
 */


class User  {

    const WAITING_VALIDATION = 0;
    const ACTIVE = 1;
    const DISABLED = 2;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    public $userId;

    /**
     * @var string
     *
     * @ORM\Column(name="login", type="string", length=180, nullable=false)
     */
    public $login;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=180, nullable=false)
     */
    public $email;
    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    public $password;

    /**
     * @var String
     *
     * @ORM\Column(name="title", type="string", nullable=true)
     */
    public $title;

    /**
     * @var String
     *
     * @ORM\Column(name="lastname", type="string", nullable=true)
     */
    public $lastname;


    /**
     * @var String
     *
     * @ORM\Column(name="firstname", type="string", nullable=true)
     */
    public $firstname;


    /**
     * @var String
     *
     * @ORM\Column(name="gender", type="string", nullable=true)
     */
    public $gender;


    /**
     * @var String
     *
     * @ORM\Column(name="picture", type="string", nullable=true)
     */
    public $picture;



    /**
     * @var String
     *
     * @ORM\Column(name="address", type="string", nullable=true)
     */
    public $address;

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin(string $login)
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getPassword() {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword(string $password)
    {
        $this->password = $password;
    }

    /**
     * @return String
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param String $title
     */
    public function setTitle(String $title)
    {
        $this->title = $title;
    }

    /**
     * @return String
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param String $lastname
     */
    public function setLastname(String $lastname)
    {
        $this->lastname = $lastname;
    }

    /**
     * @return String
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param String $firstname
     */
    public function setFirstname(String $firstname)
    {
        $this->firstname = $firstname;
    }

    /**
     * @return String
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param String $gender
     */
    public function setGender(String $gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return String
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * @param String $picture
     */
    public function setPicture(String $picture)
    {
        $this->picture = $picture;
    }

    /**
     * @return String
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param String $address
     */
    public function setAddress(String $address)
    {
        $this->address = $address;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId(int $userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }



}
