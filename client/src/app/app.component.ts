import {Component } from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  protected API_URL  =  'http://35.234.69.232/api';
  protected users = undefined;
  protected user = undefined;


  constructor(private  httpClient:  HttpClient) {}

  retrieveAll(){
    return  this.httpClient.get(this.API_URL+'/users').subscribe((data:  Array<object>) => {
      this.users  =  data['hydra:member'];
      console.log(data,this.users);
    });;
  }

  getDetails(userId) {
    return  this.httpClient.get(this.API_URL+'/users/'+userId).subscribe((data:  Array<object>) => {
      this.user  =  data;
    });;
  }

}
